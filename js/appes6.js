class Insurance { 
    constructor(brand, year, type) {
        this.brand = brand;
        this.year = year;
        this.type = type;
    }
    quoteInsurace() {
        const base = 2000;
        let price;
        const age = new Date().getFullYear() - this.year;

        switch(this.brand){
            case '1':
                price = base * 1.15;
                break;
            case '2':
                price = base * 1.05;
                break;
            case '3':
                price = base * 1.35;
                break;
        }
        price -= price * age * 0.03;

        if(this.type === 'basico'){
            price += price * 0.3;
        } else {
            price += price * 0.5;
        }
        return price;
    }
}


class Interface{
    showQuote(insurance, price){
        const result = document.getElementById('resultado');
        let brand;
        switch(insurance.brand){
            case '1':
                brand = 'Americano';
                break;
            case '2':
            brand = 'Asiatico';
                break;
            case '3':
                brand = 'Europeo';
                break;
        }
        const div = document.createElement('div');
        div.innerHTML = `
            <p class= 'header'>Resumen de cotizacion:<p/>
            Marca: ${brand}<br/>
            Año:: ${insurance.year}<br/>
            Tipo: ${insurance.type}<br/>
            Total= ${price}`;

        const spinner = document.querySelector('#cargando img');
        spinner.style.display = 'block';
        setTimeout(() => {
            spinner.style.display = 'none';
            result.appendChild(div);
        }, 2000);
    }

    showMessage(message, type) {
        const div = document.createElement('div');
        const sendBtn = document.getElementById('submit');
        if(type === 'error') {
            div.classList.add('message','error');
        } else {
            div.classList.add('message','correcto');
        }
        div.innerHTML = `${message}`;
        form.insertBefore(div, document.querySelector('.form-group'));
        sendBtn.disabled = true;
        setTimeout(function(){
            document.querySelector('.message').remove();
            sendBtn.disabled = false;
        }, 2000);
    }
}


const form = document.getElementById('cotizar-seguro');

form.addEventListener('submit', function(e) {
    e.preventDefault();
    const brand = document.getElementById('marca');
    const selectedBrand = brand.options[brand.selectedIndex].value;

    const year = document.getElementById('anio');
    const selectedYear = year.options[year.selectedIndex].value;

    const selectedType = document.querySelector('input[name="tipo"]:checked').value

    const interface = new Interface();
    const result = document.querySelector('#resultado div');

    if(selectedBrand === '' ||  selectedYear === '' || selectedType === '') {
        interface.showMessage('Loca campos deben ser diligenciados', 'error');
    } else {
        const result = document.querySelector('#resultado div');
        const insurance = new Insurance(selectedBrand, selectedYear, selectedType);
        const price = insurance.quoteInsurace(insurance);
        interface.showQuote(insurance, price);
        interface.showMessage('Cotizando...', 'exito');
    }
    if(result != null){
        result.remove();
    }
})

const max = new Date().getFullYear(), min = max - 20;
const selectYear = document.getElementById('anio');

for(let i = max; i > min; i--) {
    let option = document.createElement('option');
    option.value = i;
    option.innerHTML =i;
    selectYear.appendChild(option);
}