function Insurance(brand, year, type) { 
    this.brand = brand;
    this.year = year;
    this.type = type;
}

Insurance.prototype.quoteInsurace = function(info) {
    const base = 2000;
    let price;
    const age = new Date().getFullYear() - this.year;

    switch(this.brand){
        case '1':
            price = base * 1.15;
            break;
        case '2':
            price = base * 1.05;
            break;
        case '3':
            price = base * 1.35;
            break;
    }
    price -= price * age * 0.03;

    if(this.type === 'basico'){
        price += price * 0.3;
    } else {
        price += price * 0.5;
    }
    return price;
}

function Interface() {}

Interface.prototype.showMessage = function(message, type) {
    const div = document.createElement('div');
    if(type === 'error') {
        div.classList.add('message','error');
    } else {
        div.classList.add('message','correcto');
    }
    div.innerHTML = `${message}`;
    form.insertBefore(div, document.querySelector('.form-group'));
    setTimeout(function(){
        document.querySelector('.message').remove();
    }, 2000);
}

Interface.prototype.showQuote = function(insurance, price){
    const result = document.getElementById('resultado');
    let brand;
    switch(insurance.brand){
        case '1':
            brand = 'Americano';
            break;
        case '2':
        brand = 'Asiatico';
            break;
        case '3':
            brand = 'Europeo';
            break;
    }
    const div = document.createElement('div');
    div.innerHTML = `
        <p class= 'header'>Resumen de cotizacion:<p/>
        Marca: ${brand}<br/>
        Año:: ${insurance.year}<br/>
        Tipo: ${insurance.type}<br/>
        Total= ${price}`;

    const spinner = document.querySelector('#cargando img');
    spinner.style.display = 'block';
    setTimeout(() => {
        spinner.style.display = 'none';
        result.appendChild(div);
    }, 2000);


}

const form = document.getElementById('cotizar-seguro');

form.addEventListener('submit', function(e) {
    e.preventDefault();
    const brand = document.getElementById('marca');
    const selectedBrand = brand.options[brand.selectedIndex].value;

    const year = document.getElementById('anio');
    const selectedYear = year.options[year.selectedIndex].value;

    const selectedType = document.querySelector('input[name="tipo"]:checked').value

    const interface = new Interface();

    if(selectedBrand === '' ||  selectedYear === '' || selectedType === '') {
        interface.showMessage('Fields must be filled', 'error');
    } else {
        const result = document.querySelector('#resultado div');
        if(result != null){
            result.remove();
        }
        const insurance = new Insurance(selectedBrand, selectedYear, selectedType);
        const price = insurance.quoteInsurace(insurance);
        interface.showQuote(insurance, price);
        interface.showMessage('Cotizando...', 'exito');
    }
})

const max = new Date().getFullYear(), min = max - 20;
const selectYear = document.getElementById('anio');

for(let i = max; i > min; i--) {
    let option = document.createElement('option');
    option.value = i;
    option.innerHTML =i;
    selectYear.appendChild(option);
}